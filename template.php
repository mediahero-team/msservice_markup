<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<div class="footer_col">
	<p class="h3">Задать вопрос</p>
	<form action="#" class="feedback form-feedback-footer" name="feedback">
		<input type="hidden" value="<?=$componentPath?>" id="path_feedback"/>
		<input type="hidden" value="<?=$arParams["IBLOCK_ID"]?>" id="IBLOCK_FEEDBACK"/>
		<div class="feedback_col __col-s first">
			<label class="name_ico form_field">
				<div class="form_field_error">
					Это поле обязательно для заполнения
				</div>
				<input name="fio" placeholder="Имя" type="text" class="feedback_field __name">
			</label>						
		</div>
		<div class="feedback_col __col-s">
			<label class="email_ico form_field">
				<div class="form_field_error">
					Это поле обязательно для заполнения
				</div>
				<input name="mail" placeholder="E-mail" type="email" class="feedback_field __email">
			</label>
		</div>
		<div class="feedback_col">
			<label class="form_message form_field">
				<div class="form_field_error">
					Это поле обязательно для заполнения
				</div>
				<textarea name="mess" placeholder="Текст сообщения" rows="8" class="feedback_mess" id="feedback_message"></textarea>
			</label>
		</div>
		<div class="feedback_captcha_img">
			<?$captcha = $APPLICATION->CaptchaGetCode();?>
			<input type="hidden" name="captcha_sid" id="captcha_sid_feedback" value="<?=$captcha?>"/>
			<img width="150" height="40" src="/bitrix/tools/captcha.php?captcha_sid=<?=$captcha?>" alt="" id="captcha_img_feedback">
		</div>
		<label for="feedback_captcha form_field" class="feedback_captcha">
			<div class="form_field_error">
				Это поле обязательно для заполнения
			</div>
			<span>Введите символы с картинки</span>
			<input name="captcha" type="text" class="feedback_field __captcha" id="feedback_captcha_text">
			<button class="btn-reload btn-feedback-reload"></button>
		</label>
		<button type="submit" class="btn btn-default btn-feedback">Отправить</button>
	</form>
	<div class="feedback_aftersubmit" style="display: none;">
		Спасибо. Ваше сообщение отправлено администрации сайта.
	</div>
</div>