// (function() {
$( document ).ready(function() {
	is_in_process = false;
	$(document).ajaxStart(function(){
		is_in_process = true;
	});
	$(document).ajaxStop(function(){
		is_in_process = false;
	});
	// header slider
	$('.js-bxslider-head').bxSlider({
		auto: true,
		controls: false,
		pagerCustom: '.js-bx-pager'
	});



	// btn to-top footer
	$(".js-to-top").click(function(){
		$("html,body").animate({scrollTop:0},"slow")
	});

	// compact menu
	$('.js-btn-menu').on('click', function () {

		$(this).next('.header_nav').toggleClass('__open');
		$(this).parents('.header').toggleClass('is-static');

		if($(this).next('.header_nav').hasClass('__open')){
			$(this).next('.header_nav').stop().animate({left:'0px'},600);
		}
		 else{
			$(this).next('.header_nav').stop().animate({left:'-280px'},600);
		}

	});

	// sticky menu
	var start_pos=$('.js-sticky-menu').offset().top;
	$(window).scroll(function(){
		if ($(window).scrollTop()>start_pos) {
			if ($('.js-sticky-menu').hasClass()==false){
				$('.js-sticky-menu').addClass('fixed-block');
				setTimeout(function () {
					$('.js-sticky-menu').addClass('is-visible');
				}, 60);
				
			}
		}
		else {
			$('.js-sticky-menu').removeClass('fixed-block');
			setTimeout(function () {
				$('.js-sticky-menu').removeClass('is-visible');
			}, 100);
			$('.header_nav').removeClass('__open');
			$('.header_nav').css('left', '');
		}
	});

	// btn show map
	$('.js-show-map').on('click', function () {
		$('.modal-map').addClass('__visible');
		$('body').addClass('__overflow');
	});
	$('.js-hide-map').on('click', function () {
		$('.modal-map').removeClass('__visible');
		$('body').removeClass('__overflow');
	});



	//modal
	// $('.js-modal-open').on('click', function () {
		// $('.modal-window').show();
	// });
	// $('.js-close-modal').on('click', function () {
		// $('.modal-window').hide();
	// });

	// form
	// $('.js-form-validate input, .js-form-validate textarea').change(function(){
		// $(this).next('.placeholder').removeClass('__hide');

	  // var field = $(this).val();
	  // if(field!==''){
		// $(this).next('.placeholder').addClass('__hide');
	  // } 
	// });

	// logoparad
	$(".js-scroll-img").smoothDivScroll({
		autoScrollingMode: "always",
		autoScrollingDirection: "endlessLoopRight",
		autoScrollingStep: 1,
		autoScrollingInterval: 25 
	});

	$(".js-scroll-img").bind("mouseover", function () {
		$(this).smoothDivScroll("stopAutoScrolling");
	}).bind("mouseout", function () {
		$(this).smoothDivScroll("startAutoScrolling");
	});

});
// }());
