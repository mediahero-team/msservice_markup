var arrMarkers = [];
function mapInitialize() {
        var mapCenter = new google.maps.LatLng(51.49051799999999, 45.94426580000004);

        var mapOptions = {
            zoom: 12,
            center: mapCenter,
            mapTypeControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);


        var locations = [
            ['«МашСтройСервис», Саратов, ул. Пензенская 7',51.4926839,45.95061350000003],
            ['«МашСтройСервис», Саратов, ул. Осипова 6',51.5656906,46.02557809999996]
        ];
    

        var marker, i;
        var infowindow = new google.maps.InfoWindow();


        google.maps.event.addListener(map, 'click', function() {
            infowindow.close();
        });

        // Определяем область показа маркеров
        var latlngbounds = new google.maps.LatLngBounds();

        for (i = 0; i < locations.length; i++) {
  
            var myLatLng = new google.maps.LatLng(locations[i][1], locations[i][2]);
            //Добавляем координаты маркера в область
            latlngbounds.extend(myLatLng);

            marker = new google.maps.Marker({
                position: myLatLng,
                map: map
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                }
            })(marker, i));
    
            arrMarkers.push(marker);
        }

        map.setCenter( latlngbounds.getCenter(), map.fitBounds(latlngbounds)); 

    }

google.maps.event.addDomListener(window, 'load', mapInitialize);
    