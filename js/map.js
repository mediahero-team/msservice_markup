var markers = [];
function mapInit() {
        var mapCenter = new google.maps.LatLng(51.49051799999999, 45.94426580000004);

        var mapOptions = {
            zoom: 12,
            center: mapCenter,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(document.getElementById("map-canvas-footer"), mapOptions);


        var locations = [
            ['«МашСтройСервис», Саратов, ул. Пензенская 7',51.4926839,45.95061350000003],
            ['«МашСтройСервис», Саратов, ул. Осипова 6',51.5656906,46.02557809999996]
        ];
    

        var marker, i;
        var infowindow = new google.maps.InfoWindow();


        google.maps.event.addListener(map, 'click', function() {
            infowindow.close();
        });


        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map,
                icon: locations[i][3]
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                }
            })(marker, i));
    
            markers.push(marker);
        }

        $('.map-controls span').on('click', function () {

            $('.map-controls span').removeClass('__selected');
            $(this).addClass('__selected');

            var lat = $(this).attr('data-lat'),
                lng = $(this).attr('data-lng');

            var newlatlng = new google.maps.LatLng(lat, lng);


            map.panTo(new google.maps.LatLng(lat, lng));

        });

    }

google.maps.event.addDomListener(window, 'load', mapInit);
    