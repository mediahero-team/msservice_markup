// (function() {
$(document).ready(function () {

    $(".par_section").click(function(e){
        e.preventDefault();
    })
    $(".js-to-top").click(function(){
        $("html,body").animate({scrollTop:0},"slow")
    });


    var tabContainers = $('.js-tabs .tabs_item'); 
    tabContainers.hide().filter(':first-child').show();

    $('.js-tabs .tabs_nav a').click(function () {
        tabContainers.hide(); 
        tabContainers.filter(this.hash).show(); 
        $('.js-tabs .tabs_nav a').removeClass('__active'); 
        $(this).toggleClass('__active'); 
        return false;
    }).filter(':first-child').click();

    // News controls
    
    
    $('.js-news-controls span').filter(':first').addClass('__active');
    $('.js-news-controls span').click(function () {

        var typeCataloque = $(this).attr('data-type');

        $('.js-news-controls span').removeClass('__active');
        $(this).addClass('__active');
        
        if(typeCataloque > 0){
            $('.news_item-wr > div').removeClass('__default');
            $('.news_item-wr > div').addClass('__modified');
            $('.news_item-wr > div:even').addClass('__blue');
        } else {
            $('.news_item-wr > div').removeClass('__modified');
            $('.news_item-wr > div').addClass('__default');
            $('.news_item-wr > div:even').removeClass('__blue');
        }
    });

    // $('.js-modal-open').on('click', function () {
        // $('.modal-window').show();
    // });
    // $('.js-close-modal').on('click', function () {
        // $('.modal-window').hide();
    // });


    // $('.js-form-validate input, .js-form-validate textarea').change(function(){
        // $(this).next('.placeholder').removeClass('__hide');

      // var field = $(this).val();
      // if(field!==''){
        // $(this).next('.placeholder').addClass('__hide');
      // } 
    // });


    $('.js-show-map').on('click', function () {
        $('.modal-map').addClass('__visible');
        $('body').addClass('__overflow');
    });
    $('.js-hide-map').on('click', function () {
        $('.modal-map').removeClass('__visible');
        $('body').removeClass('__overflow');
    });

    // sticky menu
    var start_pos=$('.js-sticky-menu').offset().top;
    $(window).scroll(function(){
        if ($(window).scrollTop()>start_pos) {
            if ($('.js-sticky-menu').hasClass()==false){
                $('.js-sticky-menu').addClass('fixed-block');
                setTimeout(function () {
                    $('.js-sticky-menu').addClass('is-visible');
                }, 60);
                
            }
        }
        else {
            $('.js-sticky-menu').removeClass('fixed-block');
            setTimeout(function () {
                $('.js-sticky-menu').removeClass('is-visible');
            }, 100);
            $('.header_nav').removeClass('__open');
            $('.header_nav').css('left', '');
        }
    });


    // promo accordeon

    function close_accordion_section() {
        $('.js-accordion-btn').removeClass('active');
        $('.promo_content').slideUp(300).removeClass('open');
    }

    $('.js-accordion').on("click", ".js-accordion-btn", function(e) {
    // $('.js-accordion-btn').click(function(e) {
        // Grab current anchor value
        var currentAttrValue = $(this).attr('href');

        if($(this).hasClass('active')) 
        {
            close_accordion_section();
        }
        else 
        {
            close_accordion_section();

            // Add active class to section title
            $(this).addClass('active');
            // Open up the hidden content panel
            $('.js-accordion ' + currentAttrValue).slideDown(300).addClass('open'); 
        }

        e.preventDefault();
    });

    // fluid btn
    if ($(window).scrollTop()>="50") $(".js-fluid-btn").fadeIn("slow")
    $(window).scroll(function(){
        if ($(window).scrollTop()<="50"){
            $(".js-fluid-btn").fadeOut("slow");
        } 
        else {
            $(".js-fluid-btn").fadeIn("slow");
        }
    });

});
// }());