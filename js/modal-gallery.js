$( document ).ready(function() {

var galleryWidth = $('.modal-gallery').outerWidth();

$('.js-product-scheme img').on('click', function () {
    $('.modal-gallery').addClass('is-open').stop().animate({right:'0px'},600);
});

$('.js-close-modal-gallery').on('click', function () {
    $('.modal-gallery').removeClass('is-open').stop().animate({right: -galleryWidth},600);
});


var galleryImg,
	galleryCaption,
	galleryImgWidth;

$('.modal-gallery_pic-s a').click(function(){
	galleryImg =  $(this).attr('href');
	galleryCaption =  $(this).attr('title');
	galleryImgWidth = $(this).attr('data-width');

	$(this).parent().next('.modal-gallery_pic-b').find('img').attr('src', galleryImg);
	$('.modal-gallery_caption').html(galleryCaption);

	galleryWidth = Number(galleryImgWidth) + 50;
	$(this).parent().parent().stop().animate({width: galleryWidth},800);

	$(this).parents('.modal-gallery_pic-s').find('a').removeClass('active');
	$(this).addClass('active');
	return false;
});

});