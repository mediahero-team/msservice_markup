$( document ).ready(function() {
    var btnPrev = $('.js-gallery .gallery_btn_prev');
    var btnNext = $('.js-gallery .gallery_btn_next');
    var slider = $('.js-gallery');
    var itemWrapper = $('.js-gallery_inner');
    var item = $('.js-gallery_item');
    var total = item.length;
    var width = item.outerWidth();
    var index = 0;
    var speed = 500;
    var start = 500;

    itemWrapper.width( total * width );

    $(btnPrev).on('click', function() {

       $(slider).find(".js-gallery_inner .js-gallery_item").eq(-1).clone().prependTo($(itemWrapper)); 
       $(slider).find(itemWrapper).css({"left":"-"+ width +"px"});
       $(slider).find(".js-gallery_inner .js-gallery_item").eq(-1).remove();    
       $(slider).find(itemWrapper).stop().animate({left: "0px"}, speed); 

    });

    $(btnNext).on('click', function() {

       $(slider).find(itemWrapper).stop().animate({left: "-"+ width +"px"}, speed, function(){
        $(slider).find(".js-gallery_inner .js-gallery_item").eq(0).clone().appendTo($(itemWrapper)); 
          $(slider).find(".js-gallery_inner .js-gallery_item").eq(0).remove(); 
          $(slider).find(itemWrapper).css({"left":"0px"}); 
       }); 

    });
});